import logging
from flask import Flask

from scripts.service.login_service import blueprint_inserter

app = Flask(__name__)
app.register_blueprint(blueprint_inserter)
try:
    if __name__ == '__main__':
        app.run()
except Exception as e:
    logging.exception("Exception occurred", exc_info=True)
    print(e)
